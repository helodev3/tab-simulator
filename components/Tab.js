
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity} from 'react-native';


export default class Tab extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            tabs:["Tab 1", "Tab 2"],
            activeTab:0,

        }
    }

    onTabClick = (index) => {
        this.setState({activeTab:index})
    }
    renderTabOne = () =>{

        return(<Text>Tab 1</Text>)
    }
    renderTabTwo = () =>{

        return(<Text>Tab 2</Text>)
    }
    renderTabButton = () => {
        let view = []
        {this.state.tabs.forEach((value,index)=>{
            view.push(<TabButton index={index}
                                 activeIndex={this.state.activeTab}
                                 title={value}
                                 onPress={this.onTabClick}/>)
        })}
        return view
    }
    renderTabBody = () => {
        switch (this.state.activeTab) {
            case 0:
                return this.renderTabOne()
            case 1:
                return this.renderTabTwo()
            default:
                return this.renderTabOne()
        }
    }

    render() {
        return(
            <View style={styles.container}>
                <View style={styles.buttonsContainer}>
                    {this.renderTabButton()}

                </View>
                <View style={styles.body}>
                    {this.renderTabBody()}
                </View>

            </View>
        )
    }
}
class TabButton extends React.Component{
    constructor(props) {
        super(props);
    }
    onPress = () => {
        this.props.onPress(this.props.index)
    }
    render() {
        let style=styles.tabButton
        if (this.props.activeIndex === this.props.index){
            style=styles.activeTabButton
        }
        return(
            <TouchableOpacity style={style} onPress={this.onPress}>
                <Text>
                    {this.props.title}
                </Text>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop:50,
        width: "100%"
    },
    buttonsContainer: {
        width: "90%",
        height: 50,
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    tabButton: {
        flex:1,
        justifyContent:"center",
        alignItems: "center",
        backgroundColor:"transparent"
    },
    activeTabButton:{
        flex:1,
        justifyContent:"center",
        alignItems: "center",
        backgroundColor:"transparent",
        borderBottomWidth:3,
        borderBottomColor:"red",

    },
    body : {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        padding:10,
        borderColor: "red",
        borderWidth: 1,
        width:"90%",
        margin: 5,

    }
})